#include "primlib.h"

/* 0 4 8 C
 * 1 5 9 D
 * 2 6 A E
 * 3 7 B F
*/
typedef enum {
    BS_O = 0x0415,
    BS_L = 0x1026,
    BS_S = 0x5412,
    BS_Z = 0x1056,
    BS_I = 0x1023,
    BS_J = 0x5462,
    BS_T = 0x5419
} BlockShape;


typedef struct {
    BlockShape shape;
    unsigned int x;
    unsigned int y;
    enum color color;
} Block;

typedef enum color Square;


void onKeyDown(SDLKey key);
void onTimerTick();

void deleteFullLines();
Block createNewBlock();
void updateNextBlockDisplay();

void drawAll();
void drawBoard();
void drawCurrentBlock();
void drawBlock(Block block, int xOffset, int yOffset);
void drawSquare(int x, int y, enum color color);
void rotateCurrentBlock();

BlockShape rotateBlockShape(BlockShape initShape);
int shapeSquareX(BlockShape bs, int index);
int shapeSquareY(BlockShape bs, int index);

int isInLegalState();
int isRowRemovable(int row);

