#include "testlib.h"
#include <time.h>
#include <string.h>

#define SCREEN_HEIGHT 480
#define SCREEN_WIDTH 640
#define SCREEN_WIDTH_CHARS 80

#define SQUARE_SIZE_PX 20
#define N_ROWS 20
#define N_COLS 10

#define BOARD_X 150
#define BOARD_Y 25

#define NEXT_X 400
#define NEXT_Y 25


#define FRAME_DELAY_MS (1000 / FPS)
#define KEY_DELAY_MS 5
#define TIMER_MAX 50

Square gameBoard[N_ROWS][N_COLS];
Block currentBlock;
Block nextBlock;


int main(int argc, char* argv[]) {
    srand(time(NULL));
    if(initGraph()){
        exit(3);
    }
    memset(gameBoard, BLACK, sizeof(gameBoard));

    SDLKey lastKey = -1;
    SDLKey key;

    currentBlock = createNewBlock();
    nextBlock = createNewBlock();
    updateNextBlockDisplay();
    drawAll();
    int timerCounter = 0;
    while((key = pollkey())) {
        if(key != -1 && key != lastKey) {
            onKeyDown(key);
            lastKey = key;
        } else {
            lastKey = key;
        }
        if(timerCounter == TIMER_MAX){
            timerCounter = 0;
            onTimerTick();
        }
        timerCounter++;
        SDL_Delay(KEY_DELAY_MS);
    }
    return 0;
}

void onKeyDown(SDLKey key) {
    if(key == SDLK_SPACE) {
        rotateCurrentBlock();
    } else if(key == SDLK_RIGHT) {
        currentBlock.x++;
        if(!isInLegalState()) {
            currentBlock.x--;
        }
    } else if(key == SDLK_LEFT) {
        currentBlock.x--;
        if(!isInLegalState()) {
            currentBlock.x++;
        }
    } else if(key == SDLK_DOWN) {
        while(isInLegalState()) {
            currentBlock.y++;
        }
        currentBlock.y--;
    }
    drawAll();
}

void onTimerTick() {
    currentBlock.y++;
    if(!isInLegalState()) {
        currentBlock.y--;
        int counter;
        int x;
        int y;
        for(counter = 0 ; counter < 4 ; counter++) {
            x = currentBlock.x + shapeSquareX(currentBlock.shape, counter);
            y = currentBlock.y + shapeSquareY(currentBlock.shape, counter);
            gameBoard[y][x] = currentBlock.color;
        }
        deleteFullLines();
        currentBlock = nextBlock;
        nextBlock = createNewBlock();
        updateNextBlockDisplay();
        if(!isInLegalState()) {
            exit(0);
        }
    }
    drawAll();
}


void deleteFullLines() {
    int counter;
    for(counter = 0 ; counter < N_ROWS ; counter++) {
        if(isRowRemovable(counter)) {
            memmove(gameBoard[1], gameBoard[0], counter*sizeof(gameBoard[0]));
        }
    }
}

Block createNewBlock() {
    Block block;
    block.x = 3;
    block.y = 0;
    switch(rand()%7) {
        case 0: block.shape = BS_O;
                break;
        case 1: block.shape = BS_L;
                break;
        case 2: block.shape = BS_S;
                break;
        case 3: block.shape = BS_Z;
                break;
        case 4: block.shape = BS_I;
                break;
        case 5: block.shape = BS_J;
                break;
        case 6: block.shape = BS_T;
    }
    block.color = rand()%6+1;
    return block;
}

void updateNextBlockDisplay() {
    rect(NEXT_X, NEXT_Y, NEXT_X + 4*SQUARE_SIZE_PX, NEXT_Y + 4*SQUARE_SIZE_PX, RED);
    filledRect(NEXT_X+1, NEXT_Y+1, NEXT_X + 4*SQUARE_SIZE_PX-1, NEXT_Y + 4*SQUARE_SIZE_PX-1, BLACK);
    drawBlock(nextBlock, NEXT_X, NEXT_Y);
}


void drawAll() {
    drawBoard();
    drawCurrentBlock();
    rect(BOARD_X-1, BOARD_Y-1, BOARD_X + N_COLS * SQUARE_SIZE_PX, BOARD_Y + N_ROWS * SQUARE_SIZE_PX, RED);
    updateScreen();
}

void drawBoard() {
    int rowCounter;
    int colCounter;
    
    for(rowCounter = 0 ; rowCounter < N_ROWS ; rowCounter++) {
        for(colCounter = 0 ; colCounter < N_COLS ; colCounter++) {
            drawSquare(BOARD_X + colCounter * SQUARE_SIZE_PX, BOARD_Y + rowCounter * SQUARE_SIZE_PX, gameBoard[rowCounter][colCounter]);
        }
    }
}

void drawCurrentBlock() {
    drawBlock(currentBlock, BOARD_X + currentBlock.x * SQUARE_SIZE_PX, BOARD_Y + currentBlock.y * SQUARE_SIZE_PX);
}

void drawBlock(Block block, int xOffset, int yOffset) {
    int counter;
    int x = xOffset + shapeSquareX(block.shape, 0) * SQUARE_SIZE_PX;
    int y = yOffset + shapeSquareY(block.shape, 0) * SQUARE_SIZE_PX;
    drawSquare(x, y, WHITE);
    for(counter = 1 ; counter < 4 ; counter++) {
        x = xOffset + shapeSquareX(block.shape, counter) * SQUARE_SIZE_PX;
        y = yOffset + shapeSquareY(block.shape, counter) * SQUARE_SIZE_PX;
        drawSquare(x, y, block.color);
    }
}

void drawSquare(int x, int y, enum color color) {
    rect(x+1, y+1, x + SQUARE_SIZE_PX - 1, y + SQUARE_SIZE_PX - 1, color);
    filledRect(x+5, y+5, x + SQUARE_SIZE_PX - 5, y + SQUARE_SIZE_PX - 5, color);
}

void rotateCurrentBlock() {
    if(currentBlock.shape == BS_O) {
        return;
    }
    int prevCenterX = shapeSquareX(currentBlock.shape, 0);
    int prevCenterY = shapeSquareY(currentBlock.shape, 0);
    Block blockSave = currentBlock;
    currentBlock.shape = rotateBlockShape(currentBlock.shape);
    currentBlock.x += (prevCenterX - shapeSquareX(currentBlock.shape, 0));
    currentBlock.y += (prevCenterY - shapeSquareY(currentBlock.shape, 0));
    if(!isInLegalState()) {
        currentBlock = blockSave;
    }
}



BlockShape rotateBlockShape(BlockShape initShape){
    const int m1 = 0x3333;
    return ((m1 - (initShape & m1)) << 2) | ((initShape >> 2) & m1);
}

int shapeSquareX(BlockShape bs, int index) {
    return bs >> (((3 - index) * 4) + 2) & 3;
}

int shapeSquareY(BlockShape bs, int index) {
    return bs >> ((3 - index) * 4) & 3;
}


int isInLegalState() {
    int x;
    int y;
    int counter;
    for(counter = 0 ; counter < 4 ; counter++) {
        x = currentBlock.x + shapeSquareX(currentBlock.shape, counter);
        y = currentBlock.y + shapeSquareY(currentBlock.shape, counter);
        if(gameBoard[y][x] != BLACK || x < 0 || y < 0 ||x >= N_COLS || y >= N_ROWS){
            return 0;
        }
    }
    return 1;
}

int isRowRemovable(int row) {
    int counter;
    for(counter = 0 ; counter < N_COLS ; counter++) {
        if(gameBoard[row][counter] == BLACK) {
            return 0;
        }
    }
    return 1;
}
