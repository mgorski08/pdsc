#include <math.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <limits.h>

int charToInt(char c, int base);
bool overflows(long result, int i, int sign, int base);

//@Override
long strtol (const char *nPtr, char **endPtr, int base) {
    if (base !=0 && (base > 36 || base < 2)) {
        errno = EINVAL;
        return 0;
    }
    long result = 0;
    int sign = 1;
    while(isspace(*nPtr)) {
        nPtr++;
    }
    
    if (*nPtr == '+') {
        sign = 1;
        nPtr++;
    } else if (*nPtr == '-') {
        sign = -1;
        nPtr++;
    }
    
    if(isspace(*nPtr) || charToInt(*nPtr, 0) == -1) {
        return 0;
    }
    
    if(base == 16 && nPtr[0] == '0' && tolower(nPtr[1]) == 'x' && charToInt(nPtr[2], 16) != -1) {
        nPtr += 2;
    }
    
    if (base == 0) {
        base = 10;
        if (*nPtr == '0') {
            nPtr++;
            if (*nPtr == 'x' && charToInt(nPtr[1], 16) != -1) {
                nPtr++;
                base = 16;
            } else {
                base = 8;
            }
        }
    }

    while(true) {
        int i = charToInt(*nPtr, base);
        if(i > -1) {
            if(overflows(result, i, sign, base)) {
                errno = ERANGE;
                result = sign==1?LONG_MAX:LONG_MIN;
                while(charToInt(*nPtr, base) != -1) {
                    nPtr++;
                }
                goto end;
            }
            result *= base;
            result += sign * i;
        } else {
            goto end;
        }
        nPtr++;
    }
    
end:
    
    if(endPtr) {
        *endPtr = (char*)nPtr;
    }
    
    
    return result;
}

int charToInt(char c, int base) {
    c = tolower(c);
    int result = -1;
    if(c >= '0' && c <= '9') {
        result = c - '0';
    } else if(c >= 'a' && c <= 'z') {
        result = c - 'a' + 10;
    }
    if (base >= 2 && result >= base) {
        result = -1;
    }
    return result; 
}

bool overflows(long result, int i, int sign, int base) {
    if(sign == 1) {
        return result > (LONG_MAX - i) / base;
    } else {
        return result < (LONG_MIN + i) / base;
    }
}
