#include "primlib.h"
#include <string.h>

#define SCREEN_HEIGHT 480
#define SCREEN_WIDTH 640
#define SCREEN_WIDTH_CHARS 80

#define FPS 1000
#define N_TOWERS 10
#define N_DISKS 20

#define TOWER_WIDTH_RELATIVE 5
#define SMALL_DISK_WIDTH_RELATIVE 10
#define BIG_DISK_WIDTH_RELATIVE 90
#define DISK_HEIGHT_RELATIVE 50
#define TOWER_HEIGHT 200

#define TOWER_WIDTH (partWidth * TOWER_WIDTH_RELATIVE / 100)
#define DISK_HEIGHT (DISK_HEIGHT_RELATIVE * TOWER_HEIGHT /N_DISKS / 100)
#define FRAME_DELAY_MS (1000 / FPS)

enum Message {INVALID, WIN, EMPTY};

typedef struct {
    int x;
    int y;
    int width;
    int height;
    enum color color;
} Rectangle;

typedef struct {
    Rectangle rectangle;
    unsigned int size;
} Disk;

typedef struct {
    Rectangle pegRectangle;
    Disk* disks[N_DISKS];
    unsigned int nOfDisks;
} Tower;


Tower towers[N_TOWERS];
Disk disks[N_DISKS];
enum Message message = EMPTY;
const double partWidth = (double)SCREEN_WIDTH/N_TOWERS;


int sign(int number){
	if(number > 0) {
		return 1;
	} else if(number < 0) {
		return -1;
	}
	return 0;
}


void putText(char* string){
    int textWidth = strlen(string)*8;
    int textX = SCREEN_WIDTH/2-textWidth/2;
    textout(textX,50,string, BLUE);
}


int putRect(Rectangle rect) {
    filledRect(rect.x, SCREEN_HEIGHT-rect.y-1, rect.x + rect.width-1, SCREEN_HEIGHT-rect.y + rect.height-2, rect.color);
    return 0;
}


int drawAll() {
    int towerCounter;
    int diskCounter;
    
    filledRect(0, 0, SCREEN_WIDTH - 1, SCREEN_HEIGHT - 1, WHITE); //Background
    for(towerCounter = 0;towerCounter<N_TOWERS;towerCounter++){
        Tower* tower = &towers[towerCounter];
        putRect(tower->pegRectangle);
        for(diskCounter = 0 ; diskCounter<tower->nOfDisks ; diskCounter++) {
            putRect(tower->disks[diskCounter]->rectangle);
        }
    }
    switch(message) {
        case EMPTY:
            putText("");
            break;
        case INVALID:
            putText("Invalid move");
            break;
        case WIN:
            putText("You win!");
            break;
    }
    updateScreen();
    return 0;
}


unsigned int towerX(unsigned int n) {
    return partWidth*n+partWidth/2-TOWER_WIDTH/2;
}


int diskWidth(int n) {
    int big = BIG_DISK_WIDTH_RELATIVE*SCREEN_WIDTH/N_TOWERS/100;
    int small = SMALL_DISK_WIDTH_RELATIVE*SCREEN_WIDTH/N_TOWERS/100;
    return big-(big-small)*n/N_DISKS;
}


int createTowers() {
    int towerCounter;
    int diskCounter;

    for(towerCounter = 0 ; towerCounter<N_TOWERS ; towerCounter++) {
        towers[towerCounter].pegRectangle.height = TOWER_HEIGHT;
        towers[towerCounter].pegRectangle.width = TOWER_WIDTH;
        towers[towerCounter].pegRectangle.x = towerX(towerCounter);
        towers[towerCounter].pegRectangle.y = 200;
        towers[towerCounter].pegRectangle.color = BLUE;
        towers[towerCounter].nOfDisks = 0;
    }
    
    for(diskCounter = 0 ; diskCounter<N_DISKS ; diskCounter++) {
		towers[0].disks[diskCounter] = disks + diskCounter;
        towers[0].disks[diskCounter]->size = N_DISKS-diskCounter;
        Rectangle* diskRect = &(towers[0].disks[diskCounter]->rectangle);
        diskRect->color = (diskCounter)%6+1;
        diskRect->height = DISK_HEIGHT;
        diskRect->width = diskWidth(diskCounter);
        diskRect->x = towers[0].pegRectangle.x+(TOWER_WIDTH/2)-diskRect->width/2;
        diskRect->y = (diskCounter+1)*DISK_HEIGHT;
    }
    towers[0].nOfDisks = N_DISKS;
    return 0;
}


Disk* getDisk(unsigned int towerID, unsigned int diskID) {
	return towers[towerID].disks[diskID];
    return 0;
}


Disk* getTopDisk(unsigned int towerID){
	return getDisk(towerID, towers[towerID].nOfDisks - 1);
}


int removeTopDisk(unsigned int towerID){
	towers[towerID].nOfDisks--;
    return 0;
}


int addDisk(unsigned int towerID, Disk* disk) {
	towers[towerID].disks[towers[towerID].nOfDisks] = disk;
	towers[towerID].nOfDisks++;
    return 0;
}


unsigned int yOfNewTop(unsigned int towerID) {
    return (towers[towerID].nOfDisks + 1) * DISK_HEIGHT;
}


int moveDiskAlongAxis(Disk* disk, char axis, int dst) {
	if(axis == 'y' || axis == 'Y') {
		int step = sign(dst - disk->rectangle.y);
		while(disk->rectangle.y != dst) {
			disk->rectangle.y += step;
			drawAll();
			SDL_Delay(FRAME_DELAY_MS);
		}
	} else if(axis == 'x' || axis == 'X') {
		int step = sign(dst - disk->rectangle.x);
		while(disk->rectangle.x != dst) {
			disk->rectangle.x += step;
			drawAll();
			SDL_Delay(FRAME_DELAY_MS);
		}
	} else {
		return 1;
	}
	return 0;
}


int moveDisk(unsigned int srcTowerID, unsigned int dstTowerID) {
    message = EMPTY;
    if(srcTowerID >= N_TOWERS || dstTowerID >= N_TOWERS) return 1;
    if(towers[srcTowerID].nOfDisks==0) return 1;
    
    Disk* diskToMove = getTopDisk(srcTowerID);
    
    if(towers[dstTowerID].nOfDisks > 0){
        if(diskToMove->size > getTopDisk(dstTowerID)->size) return 1;
    }

	moveDiskAlongAxis(diskToMove, 'y', 300);
	
	removeTopDisk(srcTowerID);
    unsigned int newY = yOfNewTop(dstTowerID);
	addDisk(dstTowerID, diskToMove);
    
    unsigned int newX = towers[dstTowerID].pegRectangle.x + TOWER_WIDTH / 2 - diskToMove->rectangle.width / 2;
    moveDiskAlongAxis(diskToMove, 'x', newX);
	moveDiskAlongAxis(diskToMove, 'y', newY);
    
    int towerCounter;
    for(towerCounter=1;towerCounter<N_TOWERS;towerCounter++){
        if (towers[towerCounter].nOfDisks == N_DISKS) {
            message = WIN;
            break;
        }
    }
    return 0;
}


int main(int argc, char* argv[]) {
    if(initGraph()){
        exit(3);
    }
    unsigned int firstKey = 0;
    createTowers();
    drawAll();
    
    int key=getkey();
    while(key!=SDLK_ESCAPE) {
        if (key>='0'&&key<='9') {
            if(key == '0') {key = '0'+10;}
            if(firstKey == 0) {
                firstKey = key-'0';
            } else {
                if(moveDisk(firstKey-1,key-'0'-1)){
                    message = INVALID;
                }
                firstKey = 0;
                drawAll();
            }
        }
        key=getkey();
    }
    return 0;
}
