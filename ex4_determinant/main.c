#include <stdio.h>
#include <stdlib.h>
#include "main.h"


int main(int argc, char **argv) {
    FILE* matrixFile;
    double** matrix;
    unsigned int matrixOrder;
    
    if(argc == 1) {
        matrixFile = fopen("/dev/stdin", "r");
    } else if(argc == 2){
        matrixFile = fopen(argv[1], "r");
    } else {
        exitWithError("Wrong number of arguments.\nUsage: det [filename]");
    }
    if(matrixFile == NULL) {
        perror("Error while opening matrix file");
        exit(1);
    }
    matrixOrder = fillMatrix(matrixFile, &matrix);
    fclose(matrixFile);
    printf("%f\n", det(matrix, matrixOrder));
    freeMatrix(matrix, matrixOrder);
    return 0;
}

void printMatrix(double *matrix[], int order) {
    for(int i = 0 ; i < order ; i++) {
        for(int j = 0 ; j < order ; j++) {
            printf("%lf ", matrix[i][j]);
        }
    printf("\n");
    }
    printf("=======\n");
}

double det(double *matrix[], int order) {
    if(order == 1) {
        return matrix[0][0];
    }
    double result = 0;
    for(int colCounter = 0 ; colCounter < order ; colCounter++) {
        double** minor = minorMatrix(matrix, colCounter, order);
        result += matrix[colCounter][0]*(-2*(colCounter%2)+1)*det(minor, order-1);
        freeMatrix(minor, order-1);
    }
    return result;
}

double** minorMatrix(double *matrix[], int colSkip, unsigned int order) {
    double** minor = allocateMatrix(order-1);
    if (minor == NULL) {
        perror("Error while allocating memory");
        exit(1);
    }
    int colOffset = 0;
    for(int colCounter = 0 ; colCounter < (order-1) ; colCounter++) {
        if(colCounter == colSkip) {
            colOffset = 1;
        }
        for(int rowCounter = 1 ; rowCounter < order ; rowCounter++) {
            minor[colCounter][rowCounter-1] = matrix[colCounter + colOffset][rowCounter];
        }
    }
    return minor;
}

void exitWithError(char* message) {
    fprintf(stderr, "%s\nExiting\n", message);
    exit(1);
}

double** allocateMatrix(unsigned int order) {
    double** matrix = malloc(order*sizeof(double*));
    if(matrix == NULL) {
        return NULL;
    }
    for(int i=0 ; i < order ; i++) {
        matrix[i] = malloc(order*sizeof(double));
        if(matrix[i] == NULL) {
            return NULL;
        }
    }
    return matrix;
}

void freeMatrix(double** matrix, unsigned int order) {
    for(int i=0 ; i < order ; i++) {
        if(matrix[i]) {
            free(matrix[i]);
        }
    }
    if(matrix) {
        free(matrix);
    }
}

unsigned int fillMatrix(FILE* matrixFile, double*** matrix) {
    int matrixOrder;
    if(fscanf(matrixFile, "%d", &matrixOrder) != 1) {
        fclose(matrixFile);
        exitWithError("Matrix file malformed");
    }
    *matrix = allocateMatrix(matrixOrder);
    if (*matrix == NULL) {
        perror("Error while allocating memory");
        exit(1);
    }
    for(int colCounter = 0 ; colCounter < matrixOrder ; colCounter++) {
        for(int rowCounter = 0 ; rowCounter < matrixOrder ; rowCounter++) {
            fscanf(matrixFile, "%lf", &(*matrix)[colCounter][rowCounter]);
        }
    }
    return matrixOrder;
}
