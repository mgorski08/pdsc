void printMatrix(double *matrix[], int n);
double det(double *matrix[], int order);
double** minorMatrix(double *matrix[], int colSkip, unsigned int order);
void exitWithError(char* message);
double** allocateMatrix(unsigned int order);
void freeMatrix(double *matrix[], unsigned int order);
unsigned int fillMatrix(FILE* matrixFile, double*** matrix);
