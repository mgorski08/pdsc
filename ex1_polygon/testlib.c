#include "primlib.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#define VERTICES_NUMBER 5
#define ROTATION_ANGLE 0.05
#define SIZE_CHANGE 0.07
#define FPS 50

struct Point {
    int x;
    int y;
};

void drawChain(struct Point p[], int n){
    int i;
    int centerX = screenWidth()/2;
    int centerY = screenHeight()/2;
    for(i=1;i<n;i++){
        line(p[i-1].x+centerX, centerY-p[i-1].y, p[i].x+centerX, centerY-p[i].y, GREEN);
    }
}

void createPolygonPrecise(struct Point polygon[VERTICES_NUMBER + 1], int n, struct Point start){    
    //set first and last point
    polygon[n].x = polygon[0].x = start.x;
    polygon[n].y = polygon[0].y = start.y;
    
    //set the angle of rotation
    double fi = 2*M_PI/n;
    int i;
    for(i=1;i<n;i++){
        polygon[i].x = cos(fi*i)*start.x-sin(fi*i)*start.y;
        polygon[i].y = sin(fi*i)*start.x+cos(fi*i)*start.y;
    }
}

void createPolygonFast(struct Point polygon[VERTICES_NUMBER + 1], int n, struct Point start){
    
    //set first and last point
    polygon[n].x = polygon[0].x = start.x;
    polygon[n].y = polygon[0].y = start.y;
    
    //set the angle of rotation
    double fi = 2*M_PI/n;
    double c = cos(fi);
    double s = sin(fi);
    int i;
    for(i=1;i<n;i++){
        polygon[i].x = c*polygon[i-1].x-s*polygon[i-1].y;
        polygon[i].y = s*polygon[i-1].x+c*polygon[i-1].y;
    }
}

void pointFromPolar(struct Point* p, double angle, double radius){
    p->x = radius*cos(angle);
    p->y = radius*sin(angle);
}

int main(int argc, char* argv[]){
    struct Point startPoint;
    if(initGraph())
    {
        exit(2);
    }
    double angle = 0;
    double radiusAngle = 0;
    struct Point polygon[VERTICES_NUMBER + 1];
    while(pollkey()!=SDLK_ESCAPE){
        if(angle>(2*M_PI)) angle -= 2*M_PI;
        if(radiusAngle>(2*M_PI)) radiusAngle -= 2*M_PI;

        filledRect(0, 0, screenWidth() - 1, screenHeight() - 1, BLACK);
        pointFromPolar(&startPoint, angle, 50*(cos(radiusAngle)+1.5));
        createPolygonPrecise(polygon, VERTICES_NUMBER, startPoint);
        drawChain(polygon, VERTICES_NUMBER+1);
        updateScreen();
        angle+=ROTATION_ANGLE;
        radiusAngle+=SIZE_CHANGE;
        SDL_Delay(1000/FPS);
    }
    return 0;
}
