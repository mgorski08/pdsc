#include "ArrayList.h"

#define FIRST_NAME_MAX_SIZE 100
#define LAST_NAME_MAX_SIZE 100
#define ADDRESS_MAX_SIZE 100

typedef struct {
    char firstName[FIRST_NAME_MAX_SIZE];
    char lastName[LAST_NAME_MAX_SIZE];
    char address[ADDRESS_MAX_SIZE];
    char PESEL[12];
} Person;

typedef struct {
    unsigned int ID;
    long balance;
    Person owner;
} Account;


void addAccount();
void listAccounts();
void transfer();
void displayDetails();
int prompt(char* optionsStr, char* promptStr);
int promptSimple();
void displayOptions(ArrayList* optionsAl);

int writeAccountToFile(Account* account, bool confirmOverwrite);
int readAccountByID(Account* account, unsigned int ID);
int readAccountByFName(Account* account, char* fName, int offset);
int readAccountByLName(Account* account, char* lName, int offset);
int readAccountByAddr(Account* account, char* addr, int offset);
int readAccountAtOffset(Account* account, unsigned int offset);
void printAccountShort(Account* account);
void printAccountFull(Account* account);
