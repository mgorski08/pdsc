#include "ArrayList.h"

void newAL(ArrayList* al) {
    unsigned int defaultSize = 10;
    al->array = malloc(sizeof(*(al->array))*defaultSize);
    al->size = defaultSize;
    al->used = 0;
}

int appendAL(ArrayList* al, char* element) {
    if(al->used == al->size) {
        char** tmp = realloc(al->array, al->size*1.5);
        if(tmp) {
            al->array = tmp;
            al->size = al->size*1.5;
        } else {
            return -1;
        }
    }
    al->array[al->used] = element;
    al->used++;
    return 0;
}

void freeAL(ArrayList* al) {
    free(al->array);
}
