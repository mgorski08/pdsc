#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include <stdlib.h>
#include <stdio.h>

typedef struct {
    char** array;
    unsigned int size;
    unsigned int used;
} ArrayList;

void newAL(ArrayList* al);
int appendAL(ArrayList* al, char* element);
void freeAL(ArrayList* al);

#endif
