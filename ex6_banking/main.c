#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "main.h"
#include "ArrayList.h"

FILE* bankDBFile;

int main(int argc, char* argv[]) {
    if(argc != 2) {
        printf("Fatal error: No database file provided. [EXITING]\n");
        exit(1);
    }
    bankDBFile = fopen(argv[1], "r+b");
    if(bankDBFile == NULL) {
        printf("Fatal error: Couldn't open database file. [EXITING]\n");
        exit(1);
    }
    char choice;
    printf("Welcome to banking system.\n");
    while(true) {
        choice = prompt("a\tadd account\n"
                        "l\tlist accounts\n"
                        "t\tmake a transfer\n"
                        "d\tdisplay account details\n"
                        "q\tquit", "Command (? for help): ");
        if(choice == -1) {
            return -1;
        }
        switch(choice) {
            case 'a': addAccount(); break;
            case 'l': listAccounts(); break;
            case 't': transfer(); break;
            case 'd': displayDetails(); break;
            case 'q': return 0; break;
        }
    }
    return 0;
}

void addAccount() {
    Account account;
    printf("Enter account number: ");
    scanf("%d", &(account.ID));
    while(getchar() != '\n');
    printf("Enter first name: ");
    scanf("%99s", account.owner.firstName);
    while(getchar() != '\n');
    printf("Enter last name: ");
    scanf("%99s", account.owner.lastName);
    while(getchar() != '\n');
    printf("Enter address: ");
    fgets(account.owner.address, 99, stdin);
    account.owner.address[strcspn(account.owner.address, "\n")] = 0;
    printf("Enter PESEL: ");
    scanf("%11s", account.owner.PESEL);
    while(getchar() != '\n');
    printf("Enter balance in gorsz(without comma): ");
    scanf("%ld", &(account.balance));
    while(getchar() != '\n');
    if(writeAccountToFile(&account, true) != 0) {
        printf("Did not write to database file.\n");
    }
}

void listAccounts() {
    int offset = 0;
    Account account;
    while(readAccountAtOffset(&account, offset) == 0) {
        printAccountShort(&account);
        offset++;
    }
}

void transfer() {
    unsigned int sourceID;
    unsigned int destinationID;
    unsigned int zl;
    unsigned int gr;
    unsigned int amount;
    Account source;
    Account destination;
    printf("Enter source account number: ");
    scanf("%d", &sourceID);
    while(getchar() != '\n');
    printf("Enter destination account number: ");
    scanf("%d", &destinationID);
    while(getchar() != '\n');
    printf("Enter zlotys: ");
    scanf("%d", &zl);
    while(getchar() != '\n');
    printf("Enter grosz's: ");
    scanf("%d", &gr);
    while(getchar() != '\n');
    amount = 100*zl+gr;
    
    readAccountByID(&source, sourceID);
    readAccountByID(&destination, destinationID);
    if(source.ID && destination.ID) {
        source.balance -= amount;
        destination.balance += amount;
    
        writeAccountToFile(&source, false);
        writeAccountToFile(&destination, false);
    }
    
}

void displayDetails() {
    Account account;
    unsigned int ID;
    int offset = 0;
    char buf[100];

    switch(prompt("n\taccount number\n"
                  "f\tfirst name\n"
                  "l\tlast name\n"
                  "a\taddress", "Search by (? for help): ")) {
        case 'n': printf("Enter account number: ");
                  scanf("%d", &ID);
                  while(getchar() != '\n');
                  readAccountByID(&account, ID);
                  if(account.ID) {
                      printAccountFull(&account);
                  }
                  break;
        case 'f': printf("Enter first name: ");
                  scanf("%99s", buf);
                  while(getchar() != '\n');
                  offset = 1 + readAccountByFName(&account, buf, 0);
                  while(account.ID) {
                      printAccountFull(&account);
                      offset = 1 + readAccountByFName(&account, buf, offset);
                  }
                  break;
        case 'l': printf("Enter last name: ");
                  scanf("%99s", buf);
                  while(getchar() != '\n');
                  offset = 1 + readAccountByLName(&account, buf, 0);
                  while(account.ID) {
                      printAccountFull(&account);
                      offset = 1 + readAccountByLName(&account, buf, offset);
                  }
                  break;
        case 'a': printf("Enter address: ");
                  fgets(buf, 99, stdin);
                  buf[strcspn(buf, "\n")] = 0;
                  offset = 1 + readAccountByAddr(&account, buf, 0);
                  while(account.ID) {
                      printAccountFull(&account);
                      offset = 1 + readAccountByAddr(&account, buf, offset);
                  }
    }
}

int prompt(char* optionsStr, char* promptStr) {
    char result;
    ArrayList optionsAl;
    newAL(&optionsAl);
    
    char* oCpy = strdup(optionsStr);
    char* tmp = oCpy;
    while(true) {
        if(appendAL(&optionsAl, tmp) == -1) {
            printf("Error while displaying menu;");
            return -1;
        }
        while(*tmp != '\n' && *tmp != '\0') {
            tmp++;
        }
        if(*tmp == '\0') {
            break;
        }
        *tmp = '\0';
        tmp++;
    }
    
    while(true) {
        printf("\n%s", promptStr);
        result = promptSimple();
        if(-1 == result) {
            continue;
        }
        if('?' == result) {
            displayOptions(&optionsAl);
            continue;
        }
        for(int i = 0 ; i < optionsAl.used ; i++) {
            if(optionsAl.array[i][0] == result) {
                free(oCpy);
                freeAL(&optionsAl);
                return result;
            }
        }
        displayOptions(&optionsAl);
    }
    
}

int promptSimple() {
    int result;
    result = getchar();
    if(result == '\n') {
        return -1;
    }
    while(getchar() != '\n');
    return result;
}

void displayOptions(ArrayList* optionsAl) {
    for(int i = 0 ; i < optionsAl->used ; i++) {
        printf("%s\n", optionsAl->array[i]);
    }
    printf("?\tprint this menu\n");
}

int writeAccountToFile(Account* account, bool confirmOverwrite) {
    Account tmpAcc;
    int offset = readAccountByID(&tmpAcc, account->ID);
    if(confirmOverwrite && tmpAcc.ID == account->ID) {
        int response = -1;
        while(-1 == response) {
            printf("Account already exists. Overwrite? (y/n): ");
            response = promptSimple();
        }
        if(response != 'y' && response != 'Y') {
            printf("Aborting\n");
            return -1;
        }
    }
    fseek(bankDBFile, offset * sizeof(*account), SEEK_SET);
    size_t written = fwrite(account, sizeof(*account), 1, bankDBFile);    
    return written?0:-1;
}

int readAccountByID(Account* account, unsigned int ID) {
    int offset = 0;
    account->ID = 0;
    while(account->ID != ID) {
        if(readAccountAtOffset(account, offset) != 0) {
            return offset;
        }
        offset++;
    }
    return offset - 1;
}

int readAccountByFName(Account* account, char* fName, int offset) {
    account->owner.firstName[0] = 0;
    while(strcmp(account->owner.firstName, fName)) {
        if(readAccountAtOffset(account, offset) != 0) {
            return offset;
        }
        offset++;
    }
    return offset - 1;
}

int readAccountByLName(Account* account, char* lName, int offset) {
    account->owner.lastName[0] = 0;
    while(strcmp(account->owner.lastName, lName)) {
        if(readAccountAtOffset(account, offset) != 0) {
            return offset;
        }
        offset++;
    }
    return offset - 1;
}

int readAccountByAddr(Account* account, char* addr, int offset) {
    account->owner.address[0] = 0;
    while(strcmp(account->owner.address, addr)) {
        if(readAccountAtOffset(account, offset) != 0) {
            return offset;
        }
        offset++;
    }
    return offset - 1;
}

int readAccountAtOffset(Account* account, unsigned int offset) {
    fseek(bankDBFile, offset * sizeof(*account), SEEK_SET);
    if(fread(account, sizeof(*account), 1, bankDBFile) != 1) {
        account->ID = 0;
        return -1;
    }
    return 0;
}

void printAccountShort(Account* account) {
    printf("%d\t%s %s\n", account->ID, account->owner.firstName, account->owner.lastName);
}

void printAccountFull(Account* account) {
    printf("=======================================\n");
    printf("Account number: %d\tBalance: %.2lf\n", account->ID, account->balance/((double)100));
    printf("---------\n");
    printf("Owner name: %s %s\n", account->owner.firstName, account->owner.lastName);
    printf("PESEL: %s\nAddress: %s\n", account->owner.PESEL, account->owner.address);
    printf("=======================================\n");
}
