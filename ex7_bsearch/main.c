#include <stdlib.h>
#include <stdio.h>

int compareInt(const int* a, const int* b) {
    if(*a < *b) {
        return -1;
    } else if(*a > *b) {
        return 1;
    } else {
        return 0;
    }
}

void* bsearch(const void* key, const void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*)) {
    const void* left = base;
    const void* right = base + size * (nmemb - 1);
    const void* current = base + size * (nmemb/2);
    const void* prev = NULL;
    int comparResult;
    
    while((comparResult = compar(key, current)) != 0) {
        if(prev == current) {
            return NULL;
        }
        if(comparResult < 0) {
            right = current;
        } else {
            left = current;
        }
        prev = current;
        current = ((right - left)/(2*size))*size + left;
    }
    return current;
}

int main() {
    int a = 13;
    int array[5] = {2, 5, 8, 12, 25};
    int* found = bsearch(&a, array, 5, sizeof(int), compareInt);
    if(found) {
        printf("%d\n", found - array);
    } else {
        printf("Element not found\n");
    }
}
