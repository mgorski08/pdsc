typedef struct _Node {
    char* data;
    struct _Node* next;
} Node;

Node* init(char* data, Node* next);
unsigned int length(Node* node);
void display(Node* node);
int push(Node** node, char* data);
char* pop(Node** node);
void destroy(Node* node);
int append(Node* node, char* data);
Node* copy(Node* original);
void reverse(Node** node);
void sort(Node* node);
void swapNext(Node* node);
