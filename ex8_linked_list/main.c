#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"

int main() {
    Node* head = init("AAA", NULL);
    push(&head, "BBB");
    push(&head, "CCC");
    push(&head, "DDD");
    append(head, "123");
    append(head, "!@#");
    display(head);
    reverse(&head);
    display(head);
    sort(head);
    display(head);
    destroy(head);
    return 0;
}

// Tworzy pierwszy element listy z zadanymi danymi i pointerem do następnego elementu
Node* init(char* data, Node* next) {
    Node* head = malloc(sizeof(Node));
    if (head) {//Jesli malloc zwrocil pointer to ustawiamy dane
        head->data = data;
        head->next = next;
    }
    return head;//Jesli nie to zwracamy ewentualnego NULLa
}

//Komentarz zbedny
unsigned int length(Node* node) {
    unsigned int result = 0;
    while(node) {
        result++;
        node = node->next;
    }
    return result;
}

//Podobnie jak w length trawersujemy liste i printujemy dane
void display(Node* node) {
    unsigned int i = 0;
    while(node) {
        printf("%d. %s\n", i, node->data);
        i++;
        node = node->next;
    }
}

//Dodajemy nowy element z zadana wartoscia na POCZATEK
int push(Node** node, char* data) {
    Node* tmp = init(data, *node);//Tworzy nowy poczatek z zadanymi danymi i poinerem do następnego jako były pierwszy
    if(tmp) {//Jesli sie udało
        *node = tmp;//Ustawiamy oryginalny pointer do początku na nowy
        return 0;//Zero oznacza sukces
    } else {
        return -1;//-1 oznacza niepowodzenie, oryginalny początek zostaje taki jak był
    }
}

//Zwraca wartość pierwszego elementu i go usuwa
char* pop(Node** node) {
    char* result = (**node)->data;
    Node* tmp = *node;
    *node = (*node)->next; //Ustawiamy aktualmy pierwszy element jako były drugi
    free(tmp);
}

//Komentarz zbędny
void destroy(Node* node) {
    Node* tmp;
    while(node) {
        tmp = node;
        node = node->next;
        free(tmp);
    }
}

//Dodajemy nowy element z zadana wartoscia na KONIEC
int append(Node* node, char* data) {
    Node* newNode = init(data, NULL);
    if(newNode) {
        while(node->next) {
            node = node->next;
        }
        node->next = newNode;
        return 0;
    } else {
        return -1;
    }
}


Node* copy(Node* original) {
    Node* new;
    Node** previous = &new;
    while(original) {
        *previous = malloc(sizeof(Node));
        (*previous)->data = original->data;
        original = original->next;
        previous = &((*previous)->next);
    }
    return new;
}

void reverse(Node** node) {
    Node* current = *node;
    *node = NULL;
    while(current) {
        Node* tmp = current->next;
        current->next = *node;
        *node = current;
        current = tmp;
    }
}

void sort(Node* node) {
    Node* tmp;
    for(int i = 0 ; i < length(node) ; i++) {
        tmp = node;
        while(tmp->next) {
            if(strcmp(tmp->data, tmp->next->data) == 1) {
                swapNext(node);
            }
            tmp = tmp->next;
        }
    }
}

void swapNext(Node* node) {
    char* tmp = node->data;
    node->data = node->next->data;
    node->next->data = tmp;
}
